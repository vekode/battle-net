<?php

namespace Vekode\BattleNet\Traits;

use GuzzleHttp;

Trait GuildProfileTrait {

    public function GuildMembers()
    {

        $client = new GuzzleHttp\Client(['base_uri' => $this->api_url]);
        $response = $client->get('/wow/guild/Korgath/Spire?fields=members&locale=en_US&apikey='. $this->app_key);
        return $response->getBody();

    }


}