<?php

namespace Vekode\BattleNet\Traits;

use GuzzleHttp;

Trait CharacterClassesTrait {

    public function CharacterClasses()
    {

        $client = new GuzzleHttp\Client(['base_uri' => $this->api_url]);

        $request = $client->get('/wow/data/character/classes');

        $query = $request->getQuery();

        $query->set('locale', 'en_US');
        $query->set('apikey', $this->app_key);

        return $request->getBody();

    }


}