<?php

namespace Vekode\BattleNet;

use Vekode\BattleNet\Traits\CharacterClassesTrait;
use Vekode\BattleNet\Traits\CharacterRacesTrait;
use Vekode\BattleNet\Traits\GuildProfileTrait;

class BattleNet {

    use CharacterClassesTrait;
    use CharacterRacesTrait;
    use GuildProfileTrait;

    protected $api_url;
    protected $api_key;

    public function __construct()
    {
        $this->api_url = config('vbnet.API_URL');
        $this->app_key = config('vbnet.APP_KEY');

    }

    public function hello()
    {
        return "Hello";
    }

}