<?php

return [
    'API_URL'   => 'https://us.api.battle.net/',
    'APP_KEY'   => function_exists('env') ? env('BNET_APP_KEY', '') : '',
    'LOCALE'    => function_exists('env') ? env('BNET_APP_KEY', '') : 'en_',
];