<?php namespace Vekode\BattleNet\Facades;
use Illuminate\Support\Facades\Facade;
class BattleNet extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Vekode\BattleNet\BattleNet';
    }
}